package ua.pp.krotov;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FirstTest {



    @Test
    public void if13(){
        assertEquals(15, new App().if13(10,15,20));
    }

    @Test
    public  void jBoolTest(){
        assertEquals(true, new App().jBool(2, -1, -5));
    }

    @Test
    public  void for13Test(){
        assertEquals((float)6.100003, new App().for13(101));
    }

    @Test
    public  void while13Test(){
        assertEquals(101.00000004305022, new App().while13(101));
    }

    @Test
    public  void integer13Test(){
        assertEquals(123, new App().integer13(312));
    }


    @Test
    public  void begin13Test(){
        assertEquals(new double[]{78.5,50.24,28.26}, new App().begin13(5,4));
    }

    @Test
    public  void case13Test(){
        assertEquals(new double[]{5.0, 7.07, 3.54, 12.5}, new App().case13(5,1));
    }

    @Test
    public  void matrix59Test(){
       int[][] matr = {{0,1}, {1, 2}};
        assertEquals(new int[][]{{1,2},{0,1}}, new App().matrix59(matr));
    }

    @Test
    public void array30_Test(){
        assertEquals(new int[]{0,2,2,0,0}, new App().array30(new int[]{7, 2, 8, 6, 7}));
    }
}

package ua.pp.krotov;

import java.lang.*;

public class App {

    public int if13(int a, int b, int c) {

        int middle = 0;
        if (a > c && a < b || a < c && a > b)
            middle = a;
        else if (b > c && b < a || b < c && b > a)
            middle = b;
        else if (c > a && c < b || c < a && c > b)
            middle = c;

        return middle;
    }

    public static boolean jBool(int a, int b, int c) {
        return a > 0 || b > 0 || c > 0;
    }

    public static double[] begin13(double R1, double R2) {

        assert (R1 > R2) : "R1 < R2";

        double[] res = new double[3];
        double Pi = 3.14;

        res[0] = Pi * (R1 * R1);
        res[1] = Pi * (R2 * R2);
        res[2] = res[0] - res[1];
        res[2] = Math.rint(100.0 * res[2]) / 100.0;

        return res;
    }

    public static double[] case13(double a, int num) {

        double catechism = 0, hypotenuse = 0, height = 0, square = 0;
        double[] res = new double[4];

        switch (num) {
            case 1:
                catechism = a;
                hypotenuse = a * (float) Math.sqrt(2);
                height = hypotenuse / 2;
                square = hypotenuse * (height / 2);
                break;
            case 2:
                hypotenuse = a;
                catechism = a / (float) Math.sqrt(2);
                height = hypotenuse / 2;
                square = hypotenuse * (height / 2);
                break;
            case 3:
                height = a;
                hypotenuse = 2 * height;
                catechism = hypotenuse / (float) Math.sqrt(2);
                square = hypotenuse * (height / 2);
                break;
            case 4:
                square = a;
                height = (float) Math.sqrt(square);
                hypotenuse = 2 * height;
                catechism = hypotenuse / (float) Math.sqrt(2);
                break;
        }
        res[0] = Math.rint(100.0 * catechism) / 100.0;
        res[1] = Math.rint(100.0 * hypotenuse) / 100.0;
        res[2] = Math.rint(100.0 * height) / 100.0;
        res[3] = Math.rint(100.0 * square) / 100.0;

        return res;
    }

    public static int[] array30(int[] arr) {

        int[] res = new int[arr.length];

        int count = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1])
                res[count++] = i;
        }
        res[count] = count;

        return res;
    }

    public static float for13(int N) {

        assert (N > 0) : "N > 0";

        float sign = 1;
        float val = 1.1f;
        float sum = 0;
        for (int i = 0; i < N; ++i) {
            sum += sign * val;
            sign *= -1;
            val += 0.1;
        }
        return sum;
    }

    public static double while13(int A) {

        assert (A > 1) : "A > 1";

        double sum = 0;

        float k = 1;

        while (sum <= A) {
            sum += 1 / k;
            k++;
        }
        return sum;
    }

    public static int[][] matrix59(int[][] matr) {


        //overturning
        for (int i = 0; i < matr.length / 2; i++) {
            for (int j = 0; j < matr[i].length; j++) {
                int tmp = matr[i][j];
                matr[i][j] = matr[matr.length - i - 1][j];
                matr[matr.length - i - 1][j] = tmp;
            }
        }


        return matr;
    }

    public static int integer13(int k) {

        assert (k > 100 && k < 999) : "k > 100 && k < 999";

        int hun = k % 100;
        int ttm = (k - hun) / 100;
        hun *= 10;
        int res = hun + ttm;
        return res;
    }

    public static void main(String[] args) {
        System.out.println("Hello World!");

    }
}
